const express = require("express");
const router = express.Router();
const productController = require("../controllers/product");
const auth = require("../auth");

// Route to create a product
router.post("/", auth.verify, (req, res) => {

	const data = {
		product: req.body,
		isAdmin: auth.decode(req.headers.authorization).isAdmin
	}

	productController.addProducts(data).then(resultFromController => res.send(resultFromController))
});


// Route to retrieve a single product sample using product ID endpoint /products/:producId
router.get("/:productId", (req, res) => {
	console.log(req.params.productId);

	productController.getProduct(req.params).then(resultFromController => res.send(resultFromController));
});

// Route to update a product sample
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
});

// Route to archive sample product
router.put("/:productId/archive", auth.verify, (req, res) => {
	productController.archiveProduct(req.params, req.body).then(resultFromController => res.send(resultFromController));
})



module.exports = router;