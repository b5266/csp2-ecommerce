const User = require("../Models/User");

const Product = require("../Models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");

module.exports.registerUser = (reqBody) => {
	let newUser = new User({
		firstName : reqBody.firstName,
		email : reqBody.email,
		password : bcrypt.hashSync(reqBody.password, 10),
		isAdmin : reqBody.isAdmin,
		order : reqBody.order
	})

	// Saves the created object to our database
	return newUser.save().then((user, error) => {
		// USer registration failed
		if(error){
			return false;
		}
		// User registration successfull
		else{
			return true;
		}
	})
}

// USer authentication (/login)
module.exports.loginUser = (reqBody) => {
	return User.findOne({email : reqBody.email}).then(result => {
		// User does not exist
		if(result == null){
			return false;
		}
		// USer exists
		else{
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if(isPasswordCorrect){
				// Generate an access token
				return {access : auth.createAccessToken(result)}
			}
			// If password does not match
			else{
				return false;
			}
		}
	})
}

module.exports.settingAdmin = (data) => {

    if (data.isAdmin) {
        let updateAdmin = {
            isAdmin : true
        };

        return User.findByIdAndUpdate(data.userId, updateAdmin).then((user, error) => {
            if (error) {
                return false;
            } 
            else {

                return true;

            }

        })
    } 
    else {
		return Promise.resolve(false);
	}

}

// Controller to create an order for specific product
module.exports.createOrder = async (data) => {
	// 
	let isUserUpdated = await User.findById(data.userId).then(user => {
		// Add the courseId in the user's enrollments array
		user.order.push({productId : data.productId});

		// Save the updated user information in the database
		return user.save().then((user, error) => {
			if(error){
				return false;
			}
			else{
				return true;
			}
		})
	})
	// Add the user ID
	let isProductUpdated = await Product.findById(data.productId).then(product =>{
		//
		product.consumer.push({userId : data.userId});

		//
		return product.save().then((product, error) => {
			if (error){
				return false;
			}
			else{
				return true;
			}
		})
	})

	// Condition that will check if the user and course documents have been updated
	// User enrollments is successful
	if(isUserUpdated && isCourseUpdated){
		return true;
	}
	// User enrollments failure
	else{
		return false;
	}
}

// Retrieve All Orders (Admin)
module.exports.getOrders = (data) => {
	if(data.isAdmin) {
		return order.find({}).then(result => {
				return result;
		})
	}
	else {
		return false;
	}
};


// Retrieve My Orders (Non-Admin)
module.exports.getMyOrders = (reqParams) => {
	return user.findById(reqParams.userId).then(result => {
		return result;
	})
} 

