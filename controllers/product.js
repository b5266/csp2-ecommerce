const Product = require("../Models/Product");

const auth = require("../auth");


// Controller to create product
module.exports.addProducts = (data) => {

	// user is admin
	if(data.isAdmin) {

		// Create a new varialble "---" and instantiate a new "..." object using the mongoose model
		// request body to provide the necessary informtaion
		let newProduct = new Product({
			name : data.product.name,
			description : data.product.description,
			price : data.product.price
		});

		// Save the created object and information to the database
		return newProduct.save().then((product, error) => {
			// Creation successfull
			if(error){
				return false;
			}
			else{
				return true;
			};
		});

	// User not admin
	}
	else{
		return Promise.resolve(false);
	};
};

// Controller function to retrie a specific course
module.exports.getProduct = (reqParams) => {
	return Product.findById(reqParams.productId).then(result => {
		return result;
	})
}

// Controller to update a specific product
module.exports.updateProduct = (reqParams, reqBody) => {
	// Specify the fileds/properties of the document to be updated
	// let status = 'Product has been updated to:'
	let updateProduct = {
		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	}
	return Product.findByIdAndUpdate(reqParams.productId, updateProduct).then((
		product, error) => {
		if(error){
			return false;
		}
		else{
			return updateProduct;
		}
	})
}

// Controller to Archive a product
module.exports.archiveProduct = (reqParams) => {
	
	let updateActiveField = {
		isActive : false
	};

	return Product.findByIdAndUpdate(reqParams.productId, updateActiveField).then((
		product, error) => {
		if(error){
			return false;
		}
		else{
			return true;
		}
	})
}