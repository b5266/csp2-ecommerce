const mongoose = require("mongoose");

const productSchema = new mongoose.Schema({
	name : {
		type : String,
		required : false
	},
	description : {
		type : String,
		required : false
	},
	price : {
		type : Number,
		required : false
	},
	isActive : {
		type : Boolean,
		default : true
	},
	createdOn : {
		type : Date,
		default : new Date()
	},
	consumer : [
	{
			userId : {
				type : String,
				required : [true, "Userid is required"]
			}
	}]

});

module.exports = mongoose.model("Product", productSchema);
