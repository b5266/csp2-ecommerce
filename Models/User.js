const mongoose = require("mongoose");

const userSchema = new mongoose.Schema({
	firstName : {
		type : String,
		required : [true, "firstName is required"]
	},
	email : {
		type : String,
		required : [true, "email is required"]
	},
	password : {
		type : String,
		required : [true, "password is  equired"]
	},
	isAdmin : {
		type : Boolean,
		default : false
	},
	order : [
		{
			totalAmount : {
				type : Number,
				required : false
			},
			purchasedOn : {
				type : Date,
				default : new Date()
			},
			userId : {
				type : String,
				required : [true, "UserId is required"]
			},
			productId :	{
				type : String,
				required : [true, "Product Id is required"]
		}
	}]
});

module.exports = mongoose.model("User", userSchema);