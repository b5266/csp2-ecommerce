const mongoose = require("mongoose");

const orderSchema = new mongoose.Schema({
	totalAmount : {
		type : Number,
		required : false
	},
	purchasedOn : {
		type : Date,
		default : new Date()
	},
	userId : {
			type : String,
			required : [true, "UserId is required"]
		},
	productId :	{
			type : String,
			required : [true, "Product Id is required"]
		}
});

module.exports = mongoose.model("Order", orderSchema);
