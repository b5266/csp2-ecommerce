const express = require("express");
const mongoose = require("mongoose");

const cors = require("cors");

const userRoutes = require("./routes/user");
const productRoutes = require("./routes/product");

const app = express();

// Connect to MongoDB
mongoose.connect("mongodb+srv://admin_zandro:All!sw3ll051591@cluster0.6i70n.mongodb.net/csp2-ecommerce?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true
});

// Prompt a message for a succesful connection to database
mongoose.connection.once('open', () => console.log('Now Connected to MongoDB Atlas'));

// Access all resources at the backend
app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended:true}));

// Defines the user string
app.use("/users", userRoutes);
// Defines the "products" string to be included for all course routes in the "course" route file
app.use("/products", productRoutes);

app.listen(process.env.PORT || 4000, () => {
	console.log(`API is now on port ${process.env.PORT || 4000 }`)
});

/*- User Registration
 - User Authentication
 - JWT implementation
 - Set User as Admin functionality
Set User as Admin workflow:
1. An authenticated admin user sends a PUT request containing a JWT in its header to the /:userId/setAsAdmin endpoint.
2. API validates JWT, returns false if validation fails.
3. If validation successful, API finds user with ID matching the userId URL parameter and sets its isAdmin property to true.*/